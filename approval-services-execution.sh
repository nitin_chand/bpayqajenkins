#!/bin/bash
if [[ "$Server_IP" == "10.180.4.240_staging6" ]]; then
    serverIp="10.180.4.240"
    echo "Logging in to ${serverIp} server"
    echo "Killing all CAW services which were started manually....."
	ssh -tt qa@${serverIp} sudo pgrep -f /paytm/approval-service/approval-web/ApprovalService.jar | sudo xargs kill -15
	ssh -tt qa@${serverIp} sudo pgrep -f /paytm/approval-service/approval-scheduler/SchedulerService.jar |sudo xargs kill -15
	ssh -tt qa@${serverIp} sudo pgrep -f /approval-service/approval-notifications-listener/ApprovalListenerService.jar |sudo xargs kill -15
	sleep 15s
    if [[ "$Service" == "All" ]];
    then
    if [[ "$Action" == "Start" ]];
        then
        echo "Starting approval-notification-listener service"
        ssh -tt qa@${serverIp} sudo systemctl start approvalNotificationListener.service
        echo "Starting approval-scheduler service"
        ssh -tt qa@${serverIp} sudo systemctl start approvalScheduler.service
        echo "Starting approval-web service"
        ssh -tt qa@${serverIp} sudo systemctl start approvalWeb.service
    elif [[ "$Action" == "Stop" ]];
        then
        echo "Stopping approval-notification-listener service"
        ssh -tt qa@${serverIp} sudo systemctl stop approvalNotificationListener.service
        echo "Stopping approval-scheduler service"
        ssh -tt qa@${serverIp} sudo systemctl stop approvalScheduler.service
        echo "Stopping approval-web service"
        sudo systemctl stop approvalWeb.service
    elif ssh -tt qa@${serverIp} [[ "$Action" == "Restart" ]];
        then
        echo "Restarting approval-notification-listener service"
        ssh -tt qa@${serverIp} sudo systemctl restart approvalNotificationListener.service
        echo "Restarting approval-scheduler service"
        ssh -tt qa@${serverIp} sudo systemctl restart approvalScheduler.service
        echo "Restarting approval-web service"
        ssh -tt qa@${serverIp} sudo systemctl restart approvalWeb.service
    fi
fi
if [[ "$Service" == "approval-notification-listener" ]];
    then
    if [[ "$Action" == "Start" ]];
        then
        echo "Starting approval-notification-listener service"
        ssh -tt qa@${serverIp} sudo systemctl start approvalNotificationListener.service
    elif [[ "$Action" == "Stop" ]];
        then
        echo "Stopping approval-notification-listener service"
        ssh -tt qa@${serverIp} sudo systemctl stop approvalNotificationListener.service
    elif [[ "$Action" == "Restart" ]];
        then
        echo "Restarting approval-notification-listener service"
        ssh -tt qa@${serverIp} sudo systemctl restart approvalNotificationListener.service
    elif [[ "$Action" == "Status" ]];
        then
        echo "Showing status of approval-notification-listener service"
        ssh -tt qa@${serverIp} sudo systemctl status approvalNotificationListener.service
    fi
fi
if [[ "$Service" == "approval-scheduler" ]];
    then
    if [[ "$Action" == "Start" ]];
        then
        echo "Starting approval-scheduler service"
        ssh -tt qa@${serverIp} sudo systemctl start approvalScheduler.service
    elif [[ "$Action" == "Stop" ]];
        then
        echo "Stopping approval-scheduler service"
        ssh -tt qa@${serverIp} sudo systemctl stop approvalScheduler.service
    elif [[ "$Action" == "Restart" ]];
        then
        echo "Restarting approval-scheduler service"
        ssh -tt qa@${serverIp} sudo systemctl restart approvalScheduler.service
    elif [[ "$Action" == "Status" ]];
        then
        echo "Showing status of approval-scheduler service"
        ssh -tt qa@${serverIp} sudo systemctl status approvalScheduler.service
    fi
fi
if [[ "$Service" == "approval-web" ]];
    then
    if [[ "$Action" == "Start" ]];
        then
        echo "Starting approval-web service"
        ssh -tt qa@${serverIp} sudo systemctl start approvalWeb.service
    elif [[ "$Action" == "Stop" ]];
        then
        echo "Stopping approval-web service"
        ssh -tt qa@${serverIp} sudo systemctl stop approvalWeb.service
    elif [[ "$Action" == "Restart" ]];
        then
        echo "Restarting approval-web service"
        ssh -tt qa@${serverIp} sudo systemctl restart approvalWeb.service
    elif [[ "$Action" == "Status" ]];
        then
        echo "Showing status of approval-web service"
        ssh -tt qa@${serverIp} sudo systemctl status approvalWeb.service
    fi
fi
elif [[ "$Server_IP" == "10.180.4.240_staging6" ]]; then
    serverIp="10.180.4.240"
    echo "Logging in to ${serverIp} server"
    if [[ "$Service" == "All" ]];
    then
    if [[ "$Action" == "Start" ]];
        then
        echo "Starting approval-notification-listener service"
        ssh -tt qa@${serverIp} sudo systemctl start approvalNotificationListener.service
        echo "Starting approval-scheduler service"
        ssh -tt qa@${serverIp} sudo systemctl start approvalScheduler.service
        echo "Starting approvalWeb service"
        ssh -tt qa@${serverIp} sudo systemctl start approvalWeb.service
    elif [[ "$Action" == "Stop" ]];
        then
        echo "Stopping approval-notification-listener service"
        ssh -tt qa@${serverIp} sudo systemctl stop approvalNotificationListener.service
        echo "Stopping approval-scheduler service"
        ssh -tt qa@${serverIp} sudo systemctl stop approvalScheduler.service
        echo "Stopping approvalWeb service"
        ssh -tt qa@${serverIp} sudo systemctl stop approvalWeb.service
    elif [[ "$Action" == "Restart" ]];
        then
        echo "Restarting approval-notification-listener service"
        ssh -tt qa@${serverIp} sudo systemctl restart approvalNotificationListener.service
        echo "Restarting approval-scheduler service"
        ssh -tt qa@${serverIp} sudo systemctl restart approvalScheduler.service
        echo "Restarting approvalWeb service"
        ssh -tt qa@${serverIp} sudo systemctl restart approvalWeb.service
    fi
fi
if [[ "$Service" == "approval-notification-listener" ]];
    then
    if [[ "$Action" == "Start" ]];
        then
        echo "Starting approval-notification-listener service"
        ssh -tt qa@${serverIp} sudo systemctl start approvalNotificationListener.service
    elif [[ "$Action" == "Stop" ]];
        then
        echo "Stopping approval-notification-listener service"
        ssh -tt qa@${serverIp} sudo systemctl stop approvalNotificationListener.service
    elif [[ "$Action" == "Restart" ]];
        then
        echo "Restarting approval-notification-listener service"
        ssh -tt qa@${serverIp} sudo systemctl restart approvalNotificationListener.service
    elif [[ "$Action" == "Status" ]];
        then
        echo "Showing status of approval-notification-listener service"
        ssh -tt qa@${serverIp} sudo systemctl status approvalNotificationListener.service
    fi
fi
if [[ "$Service" == "approval-scheduler" ]];
    then
    if [[ "$Action" == "Start" ]];
        then
        echo "Starting approval-scheduler service"
        ssh -tt qa@${serverIp} sudo systemctl start approvalScheduler.service
    elif [[ "$Action" == "Stop" ]];
        then
        echo "Stopping approval-scheduler service"
        ssh -tt qa@${serverIp} sudo systemctl stop approvalScheduler.service
    elif [[ "$Action" == "Restart" ]];
        then
        echo "Restarting approval-scheduler service"
        ssh -tt qa@${serverIp} sudo systemctl restart approvalScheduler.service
    elif [[ "$Action" == "Status" ]];
        then
        echo "Showing status of approval-scheduler service"
        ssh -tt qa@${serverIp} sudo systemctl status approvalScheduler.service
    fi
fi
if [[ "$Service" == "approvalWeb" ]];
    then
    if [[ "$Action" == "Start" ]];
        then
        echo "Starting approvalWeb service"
        ssh -tt qa@${serverIp} sudo systemctl start approvalWeb.service
    elif [[ "$Action" == "Stop" ]];
        then
        echo "Stopping approvalWeb service"
        ssh -tt qa@${serverIp} sudo systemctl stop approvalWeb.service
    elif [[ "$Action" == "Restart" ]];
        then
        echo "Restarting approvalWeb service"
        ssh -tt qa@${serverIp} sudo systemctl restart approvalWeb.service
    elif [[ "$Action" == "Status" ]];
        then
        echo "Showing status of approvalWeb service"
        ssh -tt qa@${serverIp} sudo systemctl status approvalWeb.service
    fi
fi
fi
